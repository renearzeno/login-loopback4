import {DefaultCrudRepository} from '@loopback/repository';
import {Testclass, TestclassRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TestclassRepository extends DefaultCrudRepository<
  Testclass,
  typeof Testclass.prototype.id,
  TestclassRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Testclass, dataSource);
  }
}
