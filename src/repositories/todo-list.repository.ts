import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {TodoList, TodoListRelations, TodoListImage} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {TodoListImageRepository} from './todo-list-image.repository';

export class TodoListRepository extends DefaultCrudRepository<
  TodoList,
  typeof TodoList.prototype.id,
  TodoListRelations
> {

  public readonly todoListImages: HasManyRepositoryFactory<TodoListImage, typeof TodoList.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('TodoListImageRepository') protected todoListImageRepositoryGetter: Getter<TodoListImageRepository>,
  ) {
    super(TodoList, dataSource);
    this.todoListImages = this.createHasManyRepositoryFactoryFor('todoListImages', todoListImageRepositoryGetter,);
    this.registerInclusionResolver('todoListImages', this.todoListImages.inclusionResolver);
  }
}
