import {Entity, model, property} from '@loopback/repository';

@model()
export class Testclass extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  newtest: string;


  constructor(data?: Partial<Testclass>) {
    super(data);
  }
}

export interface TestclassRelations {
  // describe navigational properties here
}

export type TestclassWithRelations = Testclass & TestclassRelations;
